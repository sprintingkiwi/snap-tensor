print("Welcome to SNAP!-TensorFlow extension server")

# Server Imports:
from http.server import BaseHTTPRequestHandler, HTTPServer
from socketserver import ThreadingMixIn
import threading


# Tensor Imports:
# things we need for NLP
import nltk
nltk.download('punkt')

from nltk.stem.lancaster import LancasterStemmer
stemmer = LancasterStemmer()

# things we need for Tensorflow
import numpy as np
import tflearn
import tensorflow as tf
import random

import json
import sys
import urllib
import os
import json
import platform

# GLOBALS
model = None
words = None
classes = None

# Check intent json file
if not os.path.isfile("intents.json"):
    j = {"intents": []}
    with open('intents.json', 'w') as outfile:
        json.dump(j, outfile, sort_keys = True, indent = 4)

class Data():
    def __init__(self):
        self.ready = False
        # import our chat-bot intents file
        self.intents = json.load(open('intents.json'))
data = Data()


def clean_up_sentence(sentence):
    # tokenize the pattern
    sentence_words = nltk.word_tokenize(sentence)
    # stem each word
    sentence_words = [stemmer.stem(word.lower()) for word in sentence_words]
    return sentence_words

# return bag of words array: 0 or 1 for each word in the bag that exists in the sentence
def bow(sentence, words, show_details=False):
    # tokenize the pattern
    sentence_words = clean_up_sentence(sentence)
    # bag of words
    bag = [0]*len(words)  
    for s in sentence_words:
        for i,w in enumerate(words):
            if w == s: 
                bag[i] = 1
                if show_details:
                    print ("found in bag: %s" % w)
    return(np.array(bag))

def load_model():
    global model, classes, words

    # restore all of our data structures
    import pickle
    data = pickle.load( open( "training_data", "rb" ) )
    words = data['words']
    classes = data['classes']
    train_x = data['train_x']
    train_y = data['train_y']

    # Build neural network
    net = tflearn.input_data(shape=[None, len(train_x[0])])
    net = tflearn.fully_connected(net, 8)
    net = tflearn.fully_connected(net, 8)
    net = tflearn.fully_connected(net, len(train_y[0]), activation='softmax')
    net = tflearn.regression(net)

    # Define model and setup tensorboard
    model = tflearn.DNN(net, tensorboard_dir='tflearn_logs')

    # p = bow("is your shop open today?", words)
    # print (p)
    # print (classes)


    # load our saved model
    model.load('./model.tflearn')



# create a data structure to hold user context
context = {}

ERROR_THRESHOLD = 0.25


def classify(sentence):
    global model, classes, words
    # generate probabilities from the model
    results = model.predict([bow(sentence, words)])[0]
    # filter out predictions below a threshold
    results = [[i,r] for i,r in enumerate(results) if r>ERROR_THRESHOLD]
    # sort by strength of probability
    results.sort(key=lambda x: x[1], reverse=True)
    return_list = []
    for r in results:
        return_list.append((classes[r[0]], r[1]))
    # return tuple of intent and probability
    return return_list


# HTTPRequestHandler class
class snapHTTPServer_RequestHandler(BaseHTTPRequestHandler):

    # GET
    def do_GET(self):
            # Send response status code
            self.send_response(200)
    
            # Send headers
            #self.send_header("Content-type","text")
            self.send_header("Access-Control-Allow-Origin", "*")
            self.end_headers()

            print(self.path)
            path = []
            for p in self.path.split("/"):
                path.append(urllib.parse.unquote(p))
            print(path)

            ####################################
            ####################################
            # RESPONSES:
            response = ""
            method = path[1]

            # Find class in sentence with threshold
            if method == "find":
                intent = path[2]
                text = path[3]                
                response = "false"
                threshold = 0.6
                print("Trying to find class " + intent + " in text: " + text)
                for i in classify(text):
                    if i[0] == intent and i[1] >= threshold:
                        response = "true"
                        break

            elif method == "raw-find":
                response = classify(path[2])

            elif method == "train":
                print("Training AI model...")
                data.ready = False
                if (platform.system() == "Windows"):
                    print("On Windows")
                    # os.system("python_interpreter\python.exe tensor.py")
                    if os.path.isfile("tensor/tensor.exe"):
                        print("Using compiled tensor executable")
                        os.system("train.bat")
                    else:
                        print("Using non-compiled tensor python script")
                        if os.path.isfile("python_interpreter\python.exe"):
                            print("Using embedded interpreter")
                            os.system("python_interpreter\python.exe tensor.py")
                        else:
                            print("Using system interpreter")
                            os.system("python3 tensor.py")
                else:
                    os.system("python3 tensor.py")
                print("Reloading trained model")
                load_model()
                data.ready = True
                response = "OK"
            
            elif method == "status":
                print("Getting status info")
                if data.ready:
                    response = "ready"
                else:
                    response = "busy"

            elif method == "add-example":
                intent = path[2]
                example = path[3]
                existing = False
                intents = data.intents["intents"]
                # print(intents)
                print("Adding example " +  example + " to class " + intent)
                for i in intents:
                    if i["tag"] == intent:
                        i["patterns"].append(example)
                        existing = True
                        break
                if not existing:
                    intents.append({"tag": intent, "patterns": [example], "responses": [], "context_set": ""})
                with open('intents.json', 'w') as outfile:
                    json.dump(data.intents, outfile, sort_keys = True, indent = 4)
                response = "OK"

            elif method == "reset":
                print("Deleting trained intens")
                data.intents = {"intents": []}
                with open('intents.json', 'w') as outfile:
                    json.dump(data.intents, outfile, sort_keys = True, indent = 4)
                response = "OK"

            # Everything else
            else:
                response = "Welcome to SNAP! Tensor AI extension"
            ####################################
            ####################################

            # Send message back to client
            # Write content as utf-8 data
            print(response)
            self.wfile.write(bytes(str(response), "utf8"))
            return


class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""


def run():
    print("Starting server...")
  
    # # Server settings
    # # Choose port 8080, for port 80, which is normally used for a http server, you need root access
    # server_address = ("127.0.0.1", 3000)
    # httpd = HTTPServer(server_address, snapHTTPServer_RequestHandler)
    # print("running server...")
    # data.ready = True
    # httpd.serve_forever()

    server = ThreadedHTTPServer(("127.0.0.1", 3001), snapHTTPServer_RequestHandler)
    print("Server started, use <Ctrl-C> to stop")
    data.ready = True
    server.serve_forever()


#train()
try:
    load_model()
except:
    print("Can not load model")

run()