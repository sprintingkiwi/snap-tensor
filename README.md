# SNAP! Tensorflow extension
Use Python 3.6.7 (https://www.python.org/downloads/release/python-367/)

## Dependencies
$ pip (or pip3) install...

* pandas==0.20
* windows-curses==1.1 (Windows only)
* numpy==1.14.5
* scipy==1.4.1
* nltk==3.4.5
* tensorflow==1.10
* tflearn==0.3.2

## Build instructions
* $ set PATH=%PATH%;C:\Windows\System32\downlevel; (Windows only)
* $ pip install pyinstaller==3.3
* $ pyinstaller snap_server.py
* $ pyinstaller tensor.py
* Go to dist folder and move the "tensor" directory inside the "snap_server" directory
* Inside dist/snap_server there is the main executable "snap_server"
* Copy the file train.bat from the repo root directory inside dist/snap_server

## Installer creation instructions (for Windows)
* Go to the download page of this repo and download Offline_BUILDS zip file
* Unzip inside the snap-tensor cloned repo folder
* Install NSIS (https://nsis.sourceforge.io/Main_Page)
* Right-click the snap-tensor.nsi file inside the repo folder and select the "Compile NSIS Script"